# See https://mmark.miek.nl/post/syntax/

SRC  := $(wildcard draft-*.md)
XML  := $(patsubst %.md,%.xml,$(SRC))
HTML  := $(patsubst %.md,%.html,$(SRC))

all: $(XML) $(HTML)

%.xml: %.md
	mmark $< > $@

%.html: %.md
	mmark -html $< > $@

clean:
	rm -f draft-*.html draft-*.xml
